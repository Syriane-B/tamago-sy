// Récupérer les boutons de Game control
let start = document.getElementById('start');
let pause = document.getElementById('pause');
let restart = document.getElementById('restart');

// variables pour les compteur needs
let cptNeige = 20;
let cptFood = 20;
let cptSleep = 20;

//déclaration des timers
let timerNeige;
let timerFood;
let timerSleep;

// Récupérer le éléments du DOM
let neige = document.querySelector('#valueNeige');
let food = document.querySelector('#valueFood');
let sleep = document.querySelector('#valueSleep');

let jaugeNeige = document.getElementById('jaugeNeige');
let jaugeFood = document.getElementById('jaugeFood');
let jaugeSleep = document.getElementById('jaugeSleep');

//déclaration de la mort du Yeti
function dead () {
    clearInterval(timerNeige);
    clearInterval(timerFood);
    clearInterval(timerSleep);
    toastr.error('je suis mort', 'C\'en est fini pour moi', {
        "closeButton": true,
        "positionClass": "toast-top-full-width",
        "showDuration": "300",
        "hideDuration": "300",
        "timeOut": "5000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    });
}

//function compteur neige
function intervalNeige(dN) {
    timerNeige = setInterval(function () {
        if (cptNeige > 0) // tant qu'on est pas à 0, on fait -1
        {
            if (cptNeige === 7) //quand le compteur arrives à 7 envoyer une alerte
            {
                toastr.warning('balance la neige', 'J\'ai chaud', {
                    "progressBar": true,
                    "positionClass": "toast-top-left",
                    "showDuration": "300",
                    "hideDuration": "300",
                    "timeOut": "5000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                });
                --cptNeige;
                neige.innerHTML = cptNeige;
                jaugeNeige.style.width = `${cptNeige * 5}%`;
            }
            else {
                --cptNeige;
                neige.innerHTML = cptNeige;
                jaugeNeige.style.width = `${cptNeige * 5}%`;
            }
        }
        else // sinon meurt
        {
            dead ();
        }
    }, dN);
}

//function compteur food
function intervalFood(dF) {
    timerFood = setInterval(function () {
        if (cptFood > 0) // tant qu'on est pas à 0, on fait -1
        {
            if (cptFood === 7) //quand le compteur arrives à 7 envoyer une alerte
            {
                toastr.warning('je veux des nachos', 'J\'ai la dalle', {
                    "progressBar": true,
                    "positionClass": "toast-top-left",
                    "showDuration": "300",
                    "hideDuration": "300",
                    "timeOut": "5000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                });
                --cptFood;
                food.innerHTML = cptFood;
                jaugeFood.style.width = `${cptFood * 5}%`;
            }
            else {
                --cptFood;
                food.innerHTML = cptFood;
                jaugeFood.style.width = `${cptFood * 5}%`;
            }
        }
        else // sinon meurt
        {
            dead ();
        }
    }, dF);
}

//function compteur sleep
function intervalSleep(dS) {
    timerSleep = setInterval(function () {
        if (cptSleep > 0) // tant qu'on est pas à 0, on fait -1
        {
            if (cptSleep === 7) //quand le compteur arrives à 7 envoyer une alerte
            {
                toastr.warning('laisse moi tranquille plz', 'Dodo...', {
                    "progressBar": true,
                    "positionClass": "toast-top-left",
                    "showDuration": "300",
                    "hideDuration": "300",
                    "timeOut": "5000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                });
                --cptSleep;
                sleep.innerHTML = cptSleep;
                jaugeSleep.style.width = `${cptSleep * 5}%`;
            }
            else {
                --cptSleep;
                sleep.innerHTML = cptSleep;
                jaugeSleep.style.width = `${cptSleep * 5}%`;
            }
        }
        else // sinon meurt
        {
            dead ();
        }
    }, dS);
}

//Remplir les jauges avec la console
//evenement sur le bouton submit
let formConsole = document.getElementById('console');
//déclaration de la variable qui contient l'ordre donné par l'input (string);
let order;
function clearTxtArea() {
    document.getElementById('orderInput').value = "";
}
//evenement qui interprète le texte
formConsole.addEventListener('submit', function (e) {
    order = document.getElementById('orderInput').value;
    e.preventDefault();
    //si on tape start le jeu se lance
    if (order === 'start') {
        intervalNeige(1600);
        intervalFood(1300);
        intervalSleep(1800);
        clearTxtArea();
    }
    //si on tape pause le jeu est suspendu
    if (order === 'pause') {
        clearInterval(timerNeige);
        clearInterval(timerFood);
        clearInterval(timerSleep);
        clearTxtArea();
    }
    //si on tape restart le jeu redémare
    if (order === 'restart') {
        //réinitialisation des compteurs
        cptNeige = 20;
        cptFood = 20;
        cptSleep = 20;
        //réinitialisation des intervales
        clearInterval(timerNeige);
        clearInterval(timerFood);
        clearInterval(timerSleep);
        // écriture des compteurs
        neige.innerHTML = cptNeige;
        jaugeNeige.style.width = `${cptNeige * 5}%`;
        food.innerHTML = cptFood;
        jaugeFood.style.width = `${cptFood * 5}%`;
        sleep.innerHTML = cptSleep;
        jaugeSleep.style.width = `${cptSleep * 5}%`;
        //lancement des compteurs
        intervalNeige(1600);
        intervalFood(1300);
        intervalSleep(1800);
        clearTxtArea();
    }
    //si on tape neige dans la console on rempli la jauge neige
    if (order === 'neige') {
        cptNeige = 20;
        neige.innerHTML = cptNeige;
        jaugeNeige.style.width = `${cptNeige * 5}%`;
        clearTxtArea();
        toastr.success('je suis bien au frais', 'Merci bébé', {
            "progressBar": true,
            "positionClass": "toast-top-left",
            "showDuration": "300",
            "hideDuration": "300",
            "timeOut": "5000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        });
    }
    //si on tape nachos dans la console on rempli la jauge food
    if (order === 'nachos') {
        cptFood = 20;
        food.innerHTML = cptFood;
        jaugeFood.style.width = `${cptFood * 5}%`;
        clearTxtArea();
        toastr.success('de la bombe ces nachos', 'Merci gros', {
            "progressBar": true,
            "positionClass": "toast-top-left",
            "showDuration": "300",
            "hideDuration": "300",
            "timeOut": "5000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        });
    }
    //si on tape igloo dans la console on rempli la jauge sleep
    if (order === 'igloo') {
        cptSleep = 20;
        sleep.innerHTML = cptSleep;
        jaugeSleep.style.width = `${cptSleep * 5}%`;
        clearTxtArea();
        toastr.success('igloo de compèt\'', 'C\'est bien d\'être tranquille', {
            "progressBar": true,
            "positionClass": "toast-top-left",
            "showDuration": "300",
            "hideDuration": "300",
            "timeOut": "5000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        });
    }
    else {
        clearTxtArea();
    }
});